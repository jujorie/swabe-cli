#!/usr/bin/env node

'use strict';

const commander = require('commander');
const pkg = require('../package');

commander
    .version(pkg.version)
    .description(pkg.description);

commander
    .command('export <file>')
    .alias('ex')
    .description('Exports the config')
    .option('-t, --title <title>', 'Sets the document title')
    .option('-y, --yaml', 'Exports as YAML')
    .option('-j, --json', 'Exports as JSON')
    .action(require('../lib/export'));

commander
    .command('jar:download')
    .alias('dl')
    .description('Download swagger-codegen-cli JAR')
    .action(require('../lib/download'));

commander
    .command('generate <file>')
    .alias('gen')
    .description('Generate clients from swagger doc')
    .option('-f, --format <format>', 'Generated code format')
    .option('-o, --output <output>', 'Output path')
    .action(require('../lib/generate'));

try {
    commander.parse(process.argv);
    if (!commander.args.length) {
        commander.help();
        process.exit(1);
    }
} catch (err) {
    console.log(err.toString());
    process.exit(1);
};
