# SWABE Command Line Utilities

## Table of Contents

* [Usage](#usage)
* [Commands](#commands)

## Usage 

```
Usage: swabe [options] [command]

Swabe command line utility

Options:
  -V, --version                  output the version number
  -h, --help                     output usage information

Commands:
  export|ex [options] <file>     Exports the config
  jar:download|dl                Download swagger-codegen-cli JAR
  generate|gen [options] <file>  Generate clients from swagger doc
```

## Commands

* [export](#export)
* [jar download](#jar-download)
* [generate](#generate)

### Export

Exports the config

__Usage__

```
$ swabe export|ex [options] <file>
```

__Options__

```
-t, --title set the document title
-y, --yaml  Exports as YAML
-j, --json  Exports as JSON
```

### Jar Download

Download swagger-codegen-cli JAR

__Usage__

```
$ swabe jar:download|dl [options]
```

### Generate

Generate clients from swagger doc

__Usage__ 

```
$ swabe generate|gen [options] <file>
```

__Options__

```
-f, --format <format>  Generated code format
-o, --output <output>  Output path
```

