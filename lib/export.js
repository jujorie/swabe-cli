'use strict';

const {resolve} = require('path');
const {writeFileSync} = require('fs');
const {safeDump} = require('js-yaml');

const cleanPathQuotes = (expression) => {
    const tripleQuoteRx = /^(\s+)'(.+?)':$/gim;
    if (expression) {
        expression = expression.replace(tripleQuoteRx, '$1$2:');
    }
    return expression;
};

const cleanTripleQuote = (expression) => {
    const tripleQuoteRx = /''('.+?')''/gim;
    if (expression) {
        expression = expression.replace(tripleQuoteRx, '"$1"');
    }
    return expression;
};

/**
 * Exports
 * @param {string} file - Server definition to be used
 * @param {object} [param = {}]
 * @param {boolean} [param.json = false] Export in JSON format
 * @param {boolean} [param.yaml = true] Export in YAML format
 * @param {string} [param.title] Document title
 */
function exportFile(file, {json = false, yaml = true, title = null} = {}) {
    /**
     * @type {SwaggerServer}
     */
    const serverDef = require(resolve(file));
    const document = serverDef.document;

    if (title && title.length && document.info) {
        document.info.title = title;
    }

    console.log(`Creating '${document.info.title}'`);

    if (true === json) {
        console.log('Write JSON');
        writeFileSync(
            resolve('export.json'),
            JSON.stringify(document, null, 4)
        );
    }

    if (true === yaml) {
        console.log('Write YAML');
        let yaml = safeDump(document);
        yaml = cleanPathQuotes(yaml);
        yaml = cleanTripleQuote(yaml);

        writeFileSync(
            resolve('export.yml'),
            yaml
        );
    }
}

module.exports = exportFile;
