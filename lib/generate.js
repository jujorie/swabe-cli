'use strict';

const {resolve} = require('path');
const {spawn} = require('child_process');
const {exists} = require('fs');
const download = require('./download');

const exec = (jarFile, file, format, output) => {
    const java = spawn('java', [
        '-jar', jarFile,
        'generate',
        '-i', resolve(file),
        '-l', format,
        '-o', resolve(output)
    ]);

    java.stderr.on('data', (data) => {
        console.log(`stderr: ${data}`);
    });

    java.on('close', (code) => {
        console.log(`Generation ${code}`);
    });
};

/**
 * Generate data from doc files
 * @param {string} file - document file
 */
function generate(file, {format = 'html2', output = './generated/swagger'}) {
    const jarFile = resolve('swagger-codegen-cli.jar');
    exists(jarFile, (fileExists) => {
        if (!fileExists) {
            return download().then(() => {
                exec(jarFile, file, format, output);
            });
        }
        exec(jarFile, file, format, output);
    });
}

module.exports = generate;
