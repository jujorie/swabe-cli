'use strict';

const {get} = require('https');
const {createWriteStream, unlink} = require('fs');
const {resolve} = require('path');

const TOOLS_URL = 'https://oss.sonatype.org/content/repositories/releases/io/swagger/swagger-codegen-cli/2.4.0/swagger-codegen-cli-2.4.0.jar';

/**
 * Download the swagger codegen jar
 * @return {Promise}
 */
function download() {
    const filename = resolve('swagger-codegen-cli.jar');
    const writer = createWriteStream(filename);
    return new Promise((resolve, reject) => {
        get(TOOLS_URL, (response) => {
            console.log('FOUND', response.statusCode);
            if (response.statusCode !== 200) {
                return resolve(
                    new Error(`Response status was ${response.statusCode}`)
                );
            }
            writer.on('finish', () => {
                console.log(`Downloaded ${filename}`);
                writer.close(resolve);
            });
            writer.on('error', (err) => {
                unlink(filename);
                reject(err);
            });
            response.pipe(writer);
        })
            .on('error', (err) => {
                unlink(filename);
                reject(err);
            });
    });
}

module.exports = download;
